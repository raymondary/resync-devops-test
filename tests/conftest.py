import pytest
from app.app import application

# global scope
# def pytest_configure():
  

@pytest.fixture(autouse=True)
def client():
  application.config['TESTING'] = True

  with application.test_client() as client:
    yield client

