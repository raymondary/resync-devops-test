'''POST /arithmetic sum operation'''
def test_sum_post(client):
	payload = {
		'x': 20,
		'y': 10,
		'operation': '+'
	}
	response = client.post('/arithmetic', query_string=payload)
	expected_resp = {
		'results': 30
	}
	assert response.status_code == 200, "Status code should be 200"
	assert response.get_json() == expected_resp, "Result should be 30"


'''POST /arithmetic subtract operation'''
def test_subtract_post(client):
	payload = {
		'x': 20,
		'y': 10,
		'operation': '-'
	}
	response = client.post('/arithmetic', query_string=payload)
	expected_resp = {
		'results': 10
	}
	assert response.status_code == 200, "Status code should be 200"
	assert response.get_json() == expected_resp, "Result should be 10"


'''POST /arithmetic multiply operation'''
def test_multiply_post(client):
	payload = {
		'x': 20,
		'y': 10,
		'operation': '*'
	}
	response = client.post('/arithmetic', query_string=payload)
	expected_resp = {
		'results': 200
	}
	assert response.status_code == 200, "Status code should be 200"
	assert response.get_json() == expected_resp, "Result should be 200"


'''POST /arithmetic divide operation'''
def test_divide_post(client):
	payload = {
		'x': 20,
		'y': 10,
		'operation': '/'
	}
	response = client.post('/arithmetic', query_string=payload)
	expected_resp = {
		'results': 2
	}
	assert response.status_code == 200, "Status code should be 200"
	assert response.get_json() == expected_resp, "Result should be 2"

'''POST /arithmetic wrong type ops'''
def test_error_post(client):
	payload = {
		'x': 'aa',
		'y': 10,
		'operation': '*'
	}
	response = client.post('/arithmetic', query_string=payload)
	expected_resp = {
		'status': '400 Bad Request: The browser (or proxy) sent a request that this server could not understand.'
	}
	assert response.status_code == 200, "Status code should be 200"
	assert response.get_json() == expected_resp, "There should be an error status"
