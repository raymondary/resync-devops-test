from dataclasses import dataclass
from app.app import db

@dataclass
class UserModel(db.Model):
    """User database model"""
    
    __tablename__ = 'users'
    id: str
    name: str
    description: str

    id = db.Column(db.String(40), primary_key=True)
    name = db.Column(db.String(40))
    description = db.Column(db.String(100))

    def __init__(self, id=None, name=None, description=None):
        self.id = id
        self.name = name
        self.description = description

    def __repr__(self):
        return '<User %r>' % self.id

    def add(self):
        db.session.add(self)
        db.session.commit()

    def delete(self):
        db.session.delete(self)
        db.session.commit()

    def update(self):
        db.session.commit()
