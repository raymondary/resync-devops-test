import os
from flask import Flask, jsonify
from flask_restful import Resource, Api, reqparse
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
import json

from sqlalchemy.sql.elements import Null

application = Flask(__name__)
# application.config['BUNDLE_ERRORS'] = True
application.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

application.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://%s:%s@%s/%s' % (
    # ARGS.dbuser, ARGS.dbpass, ARGS.dbhost, ARGS.dbname
    os.environ['DBUSER'], os.environ['DBPASS'], os.environ['DBHOST'], os.environ['DBNAME']
)

# initialize the database connection
db = SQLAlchemy(application)

# initialize database migration management
migrate = Migrate(application, db)

from app.models import UserModel

api = Api(application)
CORS(application, send_wildcard=True)

with open("./users.json", "r") as f:
    users = json.load(f)

# seed command function
@application.cli.command('seed')
def seed():
    for user in users:
        user = UserModel(user['id'], user['name'], user['description'])
        user.add()
        print("%s is added" % user)

class arithmetic(Resource):
    def post(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('x', type=int, required=True, location='args', help='X Value')
            self.parser.add_argument('y', type=int, required=True, location='args', help='Y Value')
            self.parser.add_argument('operation', type=str, location='args', help='Mathematical Operators')
            self.args = self.parser.parse_args()

            x = (self.args['x'])
            y = (self.args['y'])
            ops = (self.args['operation'])

            if ops == '-':
                return {'results': x - y}
            elif ops == '*':
                return {'results': x * y}
            elif ops == '/':
                return {'results': x / y}
            else:
                return {'results': x + y}
            
        except Exception as e:
            return {'status': str(e)}

class User(Resource):
    '''Get user by id'''
    def get(self, user_id):
        user = UserModel.query.get(user_id)

        if user:
            return jsonify({
                'message': 'Success', 
                'data': user
            })
        else:
            response = jsonify({
                'message': 'Error',
                'error': 'User not found'
            })
            response.status_code = 404
            return response

    '''Update user by id'''
    def put(self, user_id):
        user = UserModel.query.get(user_id)
        self.parser = reqparse.RequestParser()
        self.parser.remove_argument('id')
        self.parser.add_argument('name', type=str, location='json', help='')
        self.parser.add_argument('description', type=str, location='json', help='')
        self.args = self.parser.parse_args()

        try:
            if not(self.args['name'] is None):
                user.name = self.args['name']
            if not(self.args['description'] is None):
                user.description = self.args['description']
            user.update()
            response = jsonify({
                'message': 'Success', 
                'data': user
            })
            response.status_code = 201
            return response

        except Exception as e:
            response = jsonify({
                'message': 'Error',
                'error': str(e)
            })
            response.status_code = 404
            return response

    '''Delete user by id'''
    def delete(self, user_id):
        try:
            user = UserModel.query.get(user_id)
            
            if user:
                user.delete()
                response = jsonify({
                    'message': 'Success',
                })
                response.status_code = 200
                return response
            else:
                response = jsonify({
                    'message': 'Error',
                    'error': 'User not found'
                })
                response.status_code = 404

        except Exception as e:
            response = jsonify({
                'message': 'Error',
                'error': str(e)
            })
            response.status_code = 500
            return response

class UserList(Resource):
    ''' List all users '''
    def get(self):
        users = UserModel.query.all()
        return jsonify({
            'message': 'Success', 
            'data': users
        })
    
    ''' Create a new user '''
    def post(self):
        try:
            self.parser = reqparse.RequestParser()
            self.parser.add_argument('id', type=str, required=True, location='json', help='id is required')
            self.parser.add_argument('name', type=str, required=True, location='json', help='name is required')
            self.parser.add_argument('description', type=str, location='json', help='description')
            self.args = self.parser.parse_args()
            user = UserModel(self.args['id'], self.args['name'], self.args['description'])
            user.add()

            response = jsonify({
                'message': 'Success', 
                'data': user
            })
            response.status_code = 201
            return response
        
        except Exception as e:
            response = jsonify({
                'message': 'Error',
                'error': str(e)
            })
            response.status_code = 400
            return response


api.add_resource(arithmetic, '/arithmetic')
api.add_resource(UserList, '/users')
api.add_resource(User, '/users/<user_id>')


if __name__ == '__main__':
    application.run(host='0.0.0.0', threaded=True)
