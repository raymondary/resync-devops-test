FROM tiangolo/uwsgi-nginx-flask:python3.8 AS build

WORKDIR /home/resync
COPY . /home/resync/
RUN pip install -r ./requirements.txt --no-cache-dir

# Build stage test - run tests
FROM build AS test

ARG DBUSER=ray
ENV DBUSER $DBUSER
ARG DBPASS=test
ENV DBPASS $DBPASS
ARG DBHOST=db
ENV DBHOST $DBHOST
ARG DBNAME=resynctest
ENV DBNAME $DBNAME

RUN pytest

# Build stage 3 - Complete the build setting the executable
FROM test AS final
ENV FLASK_APP=app/app.py
CMD flask db upgrade && flask seed && flask run -h 0.0.0.0 -p 5000
