# Resync DevOps Test

This is Resync's DevOps test project built by Python, Flask and Docker.

## Running directly on your machine
You need to have postgres server installed and running locally.
```
python3 -m venv env                 # Create python3 virtual env

source env/bin/activate             # Activate virtual env

pip install -r requirements.txt     # Install python packages in virutal env

# Flask Development Environment Variables
export FLASK_APP=app/app.py
export FLASK_ENV=development

# Postgres Server Variables 
export DBUSER=xxxx 
export DBPASS=xxxx
export DBHOST=xxxx
export DBNAME=xxxx

flask db upgrade                    # Upgrade db to be up-to-dated according to the migrations
flask seed                          # Seed the users table from users.json file
flask run                           # Run the flask app
```

Backend will be running at `localhost:3000`.

## Running on docker
```
docker-compose build                # Build docker images

docker-compose up                   # Run containers
docker-compose down                 # Stop and remove everything.

```
Backend will be running at `localhost:3000`.

## Unit Tests
Run the following:
```
pytest
```

### Documentation
* postman/Resync DevOps Test.postman_collection.json
* [Postman Documentation](https://documenter.getpostman.com/view/13286296/TzkzqeX2)
